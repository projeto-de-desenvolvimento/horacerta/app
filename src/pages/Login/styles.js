import { StyleSheet } from 'react-native';
import { not } from 'react-native-reanimated';

export default StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#6AB4B9',
    },
    


    viewLogo: {
       alignItems: 'center',
        justifyContent:'center',
        width:500,
        height: 100,
        display: 'flex',
        flexDirection: 'row',
        
        marginBottom: 35,

    },
    logo:{
        width: 70,
        height: 70,
        
    },
    title: {
        color: 'white',
        
        marginLeft: 10,
        fontSize: 22,
    },
    txtCerta: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 22,
    },

    input: {
        width: '90%',
        height: 50,
        marginBottom: 15,
        fontSize: 17,
        fontWeight: 'bold',
        borderRadius: 40,
        padding: 10,
        fontSize: 18,
        textAlign: 'center',
        borderBottomColor: 'white',
        borderTopWidth: 2,
        borderLeftWidth: 2,
        borderRightWidth: 2,
        borderBottomWidth: 2,
        borderTopColor: 'white',
        borderLeftColor: 'white',
        borderRightColor: 'white',
        borderEndWidth: 0,
    },

    btnSubmit: {
        backgroundColor: 'white',
        width: '50%',
        height: 50,
        textAlign: 'center',
        justifyContent: 'center',
        borderRadius: 40,

    },

    textButtonS: {
        fontSize: 18,
        color: '#6AB4B9',
        fontWeight: 'bold',
        textAlign: 'center',
        
    },
    btnRegister: {
        marginTop: 10,
    },

    textButtonR: {
        fontSize: 18,
        color: '#000',
        fontWeight: 'bold',
    },

});