import React, { useState, useEffect } from 'react';
import {
    View,
    KeyboardAvoidingView,
    Text,
    TouchableOpacity,
    Image,
    ImageBackground,
    TextInput,
    Animated,
    Keyboard,
    Alert
} from 'react-native';
import styles from './styles';
import api from '../../services/api';
//import AsyncStorage from '@react-native-community/async-storage';
import { LongPressGestureHandler } from 'react-native-gesture-handler';





export default function Login({ navigation }) {

    

    const [cpf, setCpf] = useState('04502712352');
    const [senha, setSenha] = useState('kaue123');
    

    function validade() {
        if (!senha && !cpf) {
            Alert.alert('Falha no login', 'Preencha o CPF e Senha corretamente!')
            return false
        }
        if (!cpf) {
            Alert.alert('Falha no login', 'Preencha seu CPF')
            return false
        }
        if (!senha) {
            Alert.alert('Falha no login', 'Preencha sua senha')
            return false
        }
        return true
    }

    function doLogin() {
        if (validade() !== false) {

        const payload = {
            cpf,
            senha
        }
        api.post('/colaboradores/login', payload).then(({ data }) => {
            
            navigation.navigate("Home")
            console.log(data.token)
            console.log(data.id)
            window.idcolaboradores = data.id
        }).catch(err => alert('Erro no login'))
    }








        // if (validade() !== false) {
        //     try {


        //         await api.post('/colaboradores/login', {

        //             body: {
        //                 cpf,
        //                 senha,
        //             }

        //         })

        //         navigation.navigate("Home")
        //         // await AsyncStorage.setItem("userId", data.id.toString());
        //         //await AsyncStorage.setItem("token", data.token);



        //     } catch (error) {
        //         alert("Login Incorreto")
        //     }
        // }

    }

    

    return (
        <KeyboardAvoidingView style={styles.background}>
            <View style={styles.viewLogo}>
                <Image source={require('../../assets/logo.png')} style={styles.logo}/>
                <Text style={styles.title}>Hora</Text><Text style={styles.txtCerta}> Certa</Text>
            </View>
           
                    <View marginRight={20} marginLeft={15} flexDirection="row">
                        
                        <TextInput style={styles.input}
                            placeholder="CPF"
                            placeholderTextColor = "white"
                            autoCorrect={false}
                            onChangeText={(e) => { setCpf(e) }}
                        />
                    </View>

                    <View marginRight={20} marginLeft={15} flexDirection="row">
                        
                        <TextInput style={styles.input}
                            placeholder="Senha"
                            placeholderTextColor = "white"
                            secureTextEntry={true}
                            autoCorrect={false}
                            onChangeText={(e) => { setSenha(e) }}
                        />
                    </View>
                    <TouchableOpacity style={styles.btnSubmit} onPress={() => doLogin()}>
                        <Text style={styles.textButtonS} >Entrar</Text>
                    </TouchableOpacity>

            
        </KeyboardAvoidingView>
    )
}

