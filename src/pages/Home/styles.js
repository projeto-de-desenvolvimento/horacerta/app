import { StyleSheet } from 'react-native';
import { Left } from 'native-base';

export default StyleSheet.create({

    container: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        backgroundColor: '#6AB4B9'
    },


    info: {
        alignItems: 'center',
        marginTop: 10,
        width: 370,
        height: 200,
        display: 'flex',
        borderRadius: 10,
        backgroundColor: 'white'
    },
    txtWifi: {
        borderRadius: 10,
        textAlign: 'center',
        width: 60,
        backgroundColor: '#6AB4B9',
        fontWeight: 'bold',
        marginTop: 20,
        fontSize: 25,
    },
    txtWifii: {
        marginTop: 5,
        fontSize: 20,
    },
    txtlocaliza: {
        borderRadius: 10,
        textAlign: 'center',
        width: 120,
        backgroundColor: '#6AB4B9',
        fontWeight: 'bold',
        marginTop: 15,
        fontSize: 20,
    },
    logo: {
        marginTop: 2,
        width: 35,
        height: 35,
    },
    viewlocaliza:{
        display: 'flex',
        
    },
    viewTwo: {
        flexDirection: 'row'
    },
    btnSubmit: {
        borderRadius: 20,
        marginTop: 20,
        backgroundColor: 'white',
        width: 200,
        height: 40,
    },
    textButtonS: {
        color: '#6AB4B9',
        fontWeight: 'bold',
        marginTop: 5,
        fontSize: 18,
        textAlign: 'center'
    }
    
});